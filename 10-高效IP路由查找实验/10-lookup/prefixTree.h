#ifndef _PREFIX_TREE_H
#define _PREFIX_TREE_H

#include <iostream>
using namespace std;
#include <fstream>
#include <cstdio>
#include <string.h>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>

#define IP_MAXLEN 16
#define BIT_NUM 32
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
#define MAX_IP_NUMS 700000

u32 StrToIp(const char *s);
size_t physical_memory_used_by_process();

// one bit prefix tree
class PTree
{
public:
	u32 SubNet;
	u8 PreLen;
	u8 Port;
	PTree()
	{
		SubNet = 0;
		PreLen = 0;
		Port = 0;
		Child[0] = Child[1] = NULL;
	};
	virtual bool ConstructTree();
	bool Insert(PTree *Node);
	u8 Search(u32 IP);
	virtual bool DestructTree();
	virtual ~PTree(){};

private:
	PTree *Child[2];
};

// two bits prefix tree
class MulPTree : public PTree
{
public:
	MulPTree *mChild[4];
	MulPTree()
	{
		SubNet = 0;
		PreLen = 0;
		Port = 0;
		mChild[0] = mChild[1] = mChild[2] = mChild[3] = NULL;
	};
	virtual bool ConstructTree();
	bool Insert(MulPTree *Node);
	u8 Search(u32 IP);
	virtual bool DestructTree();
	virtual ~MulPTree(){};
};

#endif
