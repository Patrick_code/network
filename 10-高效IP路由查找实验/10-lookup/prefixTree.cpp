#include "prefixTree.h"

u32 StrToIp(const char *s)
{
	u8 loc[4];
	u32 num = 0;
	loc[0] = 0;
	const char *tmp = s;
	while (*tmp)
	{
		if (*tmp == '.')
		{
			loc[++num] = tmp - s;
		}
		++tmp;
	}
	std::string tmps(s);
	u8 ip4[4];
	ip4[0] = atoi(tmps.substr(0, loc[1]).c_str());
	for (int i = 1; i != 4; ++i)
	{
		ip4[i] = atoi(tmps.substr(loc[i] + 1, loc[i + 1] - loc[i] - 1).c_str());
	}
	u32 ret = 0;
	u64 base = 1;
	for (int i = 3; i >= 0; --i)
	{
		ret += ip4[i] * base;
		base *= 256;
	}
	return ret;
}

size_t physical_memory_used_by_process()
{
	FILE *file = fopen("/proc/self/status", "r");
	int result = -1;
	char line[128];

	while (fgets(line, 128, file) != nullptr)
	{
		if (strncmp(line, "VmRSS:", 6) == 0)
		{
			int len = strlen(line);

			const char *p = line;
			for (; std::isdigit(*p) == false; ++p)
			{
			}

			line[len - 3] = 0;
			result = atoi(p);

			break;
		}
	}
	fclose(file);
	return result;
}

// one bit prefix tree
bool PTree::ConstructTree()
{
	std::ifstream infile;
	infile.open("./forwarding-table.txt");
	if (!infile)
	{
		std::cout << "Open File Error.\n";
		return false;
	}
	std::string s;
	while (getline(infile, s))
	{
		u16 fb = s.find(' ');
		u16 lb = s.find_last_of(' ');
		PTree *Node = new PTree();
		Node->SubNet = StrToIp(s.substr(0, fb).c_str());
		Node->PreLen = atoi(s.substr(fb + 1, lb).c_str());
		Node->Port = atoi(s.substr(lb + 1).c_str());
		if (!Insert(Node))
		{
			infile.close();
			return false;
		}
	}
	infile.close();
	return true;
}

bool PTree::Insert(PTree *Node)
{
	PTree *pos = this, *q, *p;
	q = p = NULL;
	u32 nodeIp = Node->SubNet;
	u8 preLen = Node->PreLen;
	bool loc;
	while (pos && preLen)
	{
		loc = nodeIp & 0x80000000;
		q = pos;
		pos = pos->Child[loc];
		nodeIp <<= 1;
		--preLen;
	}
	if (!preLen)
	{
		if (!pos)
		{
			q->Child[loc] = Node;
		}
		else
		{
			pos->SubNet = Node->SubNet;
			pos->PreLen = Node->PreLen;
			pos->Port = Node->Port;
			delete Node;
		}
	}
	else
	{
		while (preLen--)
		{
			p = new PTree();
			q->Child[loc] = p;
			q = q->Child[loc];
			loc = nodeIp & 0x80000000;
			nodeIp <<= 1;
		}
		p->Child[loc] = Node;
	}
	return true;
}

u8 PTree::Search(u32 IP)
{
	PTree *pos = this, *q = NULL;
	u32 tmp = IP;
	bool loc;
	while (pos)
	{
		loc = tmp & 0x80000000;
		q = pos;
		pos = pos->Child[loc];
		tmp <<= 1;
	}
	int bite = 0;
	for (int i = 0; i <= q->PreLen; ++i)
	{
		if (q->SubNet >> (31 - i) == IP >> (31 - i))
			++bite;
		else
		{
			break;
		}
	}
	if (bite < q->PreLen)
	{
		return this->Port;
	}
	return q->Port;
}

bool PTree::DestructTree()
{
	u8 i;
	for (i = 0; i != 2; ++i)
	{
		if (Child[i])
		{
			Child[i]->DestructTree();
		}
	}
	delete this;
	return true;
}

// two bits prefix tree
bool MulPTree::ConstructTree()
{
	std::ifstream infile;
	infile.open("./forwarding-table.txt");
	if (!infile)
	{
		std::cout << "Open File Error.\n";
		return false;
	}
	std::string s;
	while (getline(infile, s))
	{
		u16 fb = s.find(' ');
		u16 lb = s.find_last_of(' ');
		MulPTree *Node = new MulPTree();
		Node->SubNet = StrToIp(s.substr(0, fb).c_str());
		Node->PreLen = atoi(s.substr(fb + 1, lb).c_str());
		Node->Port = atoi(s.substr(lb + 1).c_str());
		if (!Insert(Node))
		{
			infile.close();
			return false;
		}
	}
	infile.close();
	return true;
}

bool MulPTree::Insert(MulPTree *Node)
{
	MulPTree *pos = this, *p, *q;
	p = q = NULL;
	u32 nodeIp = Node->SubNet;
	int preLen = Node->PreLen;
	u8 loc;
	while (pos && preLen > 1)
	{
		loc = (nodeIp & 0xc0000000) >> 30;
		q = pos;
		pos = pos->mChild[loc];
		nodeIp <<= 2;
		preLen -= 2;
	}
	if (preLen == 0)
	{
		if (pos)
		{
			pos->SubNet = Node->SubNet;
			pos->PreLen = Node->PreLen;
			pos->Port = Node->Port;
			delete Node;
		}
		else
		{
			q->mChild[loc] = Node;
		}
	}
	else if (preLen > 1)
	{
		while (preLen > 0)
		{
			p = new MulPTree();
			q->mChild[loc] = p;
			q = q->mChild[loc];
			loc = (nodeIp & 0xc0000000) >> 30;
			nodeIp <<= 2;
			preLen -= 2;
		}
		if (preLen == 0)
		{
			p->mChild[loc] = Node;
		}
		else
		{
			MulPTree *NewNode = new MulPTree();
			NewNode->SubNet = Node->SubNet;
			NewNode->PreLen = Node->PreLen;
			NewNode->Port = Node->Port;
			if (loc & 0x2)
			{
				p->mChild[2] = Node;
				p->mChild[3] = NewNode;
			}
			else
			{
				p->mChild[0] = Node;
				p->mChild[1] = NewNode;
			}
		}
	}
	else
	{
		if (!pos)
		{
			q->mChild[loc] = new MulPTree();
			pos = q->mChild[loc];
		}
		MulPTree *NewNode = new MulPTree();
		NewNode->SubNet = Node->SubNet;
		NewNode->PreLen = Node->PreLen;
		NewNode->Port = Node->Port;
		loc = (nodeIp & 0xc0000000) >> 30;
		if (loc & 0x2)
		{
			if (!pos->mChild[2])
			{
				pos->mChild[2] = Node;
			}
			if (!pos->mChild[3])
			{
				pos->mChild[3] = NewNode;
			}
		}
		else
		{
			if (!pos->mChild[0])
			{
				pos->mChild[0] = Node;
			}
			if (!pos->mChild[1])
			{
				pos->mChild[1] = NewNode;
			}
		}
	}
	return true;
}

u8 MulPTree::Search(u32 IP)
{
	MulPTree *pos = this, *q = NULL;
	u32 tmp = IP;
	u8 loc;
	while (pos)
	{
		loc = (tmp & 0xc0000000) >> 30;
		q = pos;
		pos = pos->mChild[loc];
		tmp <<= 2;
	}
	int bite = 0;
	for (int i = 0; i <= q->PreLen; ++i)
	{
		if (q->SubNet >> (31 - i) == IP >> (31 - i))
			++bite;
		else
		{
			break;
		}
	}
	if (bite < q->PreLen)
	{
		return this->Port;
	}
	return q->Port;
}

bool MulPTree::DestructTree()
{
	u8 loc;
	for (loc = 0; loc != 4; ++loc)
	{
		if (mChild[loc])
		{
			mChild[loc]->DestructTree();
		}
	}
	delete this;
	return true;
}
