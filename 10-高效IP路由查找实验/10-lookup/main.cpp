#include "prefixTree.h"

int main()
{
	// init get ip info for test
	int num = 0;
	u32 ip[MAX_IP_NUMS];
	std::ifstream infile;
	std::string s;
	infile.open("./forwarding-table.txt");
	if (!infile)
	{
		std::cout << "Open File Error.\n";
		return false;
	}
	while (getline(infile, s))
	{
		u16 fb = s.find(' ');
		ip[num] = StrToIp(s.substr(0, fb).c_str());
		num++;
	}
	infile.close();
	cout << "\nInput search IP num: " << num << endl;
	cout << endl;

	int i;
	struct timeval start, end;
	double time;
	u8 one_bit_result[MAX_IP_NUMS];
	u8 two_bits_result[MAX_IP_NUMS];

	// one bit prefix tree test
	PTree *Tree1 = new PTree();
	cout << "Constructing one bit prefix tree...\n";
	if (!Tree1->ConstructTree())
	{
		std::cout << "Construct one bit prefix tree failed.\n";
		return 0;
	}
	i = 0;
	gettimeofday(&start, NULL);
	while (i < num)
	{
		one_bit_result[i] = Tree1->Search(ip[i]);
		i++;
	}
	gettimeofday(&end, NULL);
	time = (end.tv_sec - start.tv_sec) * 1000000.0 + (end.tv_usec - start.tv_usec);
	cout << "One bit prefix tree total timeuse = " << time << " us\n";
	cout << "One bit prefix tree avarange timeuse = " << time / num << " us\n";
	cout << "memory usage = " << physical_memory_used_by_process() << " kb\n\n";
	Tree1->DestructTree();

	// two bits prefix tree test
	MulPTree *Tree2 = new MulPTree();
	cout << "Constructing two bits prefix tree...\n";
	if (!Tree2->ConstructTree())
	{
		std::cout << "Construct two bits prefix tree failed.\n";
		return 0;
	}
	i = 0;
	gettimeofday(&start, NULL);
	while (i < num)
	{
		two_bits_result[i] = Tree2->Search(ip[i]);
		i++;
	}
	gettimeofday(&end, NULL);
	time = (end.tv_sec - start.tv_sec) * 1000000.0 + (end.tv_usec - start.tv_usec);
	cout << "Two bits prefix tree total timeuse = " << time << " us\n";
	cout << "Two bits prefix tree avarange timeuse = " << time / num << " us\n";
	cout << "memory usage = " << physical_memory_used_by_process() << " kb\n\n";
	Tree2->DestructTree();

	// check test results
	i = 0;
	while (i < num)
	{
		// if (one_bit_result[i] != two_bits_result[i]) {
		// 	u8 ip1 = ip[i] / (256*256*256);
		// 	ip[i] = ip[i] % (256*256*256);
		// 	u8 ip2 = ip[i] / (256*256);
		// 	ip[i] = ip[i] % (256*256);
		// 	u8 ip3 = ip[i] / 256;
		// 	u8 ip4 = ip[i] % 256;
		// 	printf("index %d, ip: %d.%d.%d.%d\n", i, ip1, ip2, ip3, ip4);
		// 	cout << "One bit prefix tree Result: " << (int)one_bit_result[i] << endl;
		// 	cout << "Two bits prefix tree result: " << (int)two_bits_result[i] << endl;
		// }
		i++;
	}
	if (i == num)
	{
		cout << "Check results end." << endl;
	}
	return 0;
}