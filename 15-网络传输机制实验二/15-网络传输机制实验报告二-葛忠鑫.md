# <center> 网络传输机制实验报告二 </center>

<center> 葛忠鑫 2018K8009929012 </center>

## 一、实验内容

### 实验内容一

- 运行给定网络拓扑 (tcp_topo.py)

- 在节点 h1 上执行 TCP 程序

    * 执行脚本 (disable_tcp_rst.sh, disable_offloading.sh)，禁止协议栈的相应功能

    * 在 h1 上运行 TCP 协议栈的服务器模式  (./tcp_stack server 10001)

- 在节点 h2 上执行 TCP 程序

    * 执行脚本 (disable_tcp_rst.sh, disable_offloading.sh)，禁止协议栈的相应功能

    * 在 h2 上运行 TCP 协议栈的客户端模式，连接 h1 并正确收发数据  (./tcp_stack client 10.0.0.1 10001)

        - client 向 server 发送数据，server 将数据 echo 给 client

- 使用 tcp_stack.py 替换其中任意一端，对端都能正确收发数据

### 实验内容二

- 修改 tcp_apps.c (以及 tcp_stack.py)，使之能够收发文件

- 执行 create_randfile.sh，生成待传输数据文件 client-input.dat

- 运行给定网络拓扑 (tcp_topo.py)

- 在节点 h1 上执行 TCP 程序

    * 执行脚本 (disable_tcp_rst.sh, disable_offloading.sh)，禁止协议栈的相应功能

    * 在 h1 上运行 TCP 协议栈的服务器模式  (./tcp_stack server 10001)

- 在节点 h2 上执行 TCP 程序

    * 执行脚本 (disable_tcp_rst.sh, disable_offloading.sh)，禁止协议栈的相应功能

    * 在 h2 上运行 TCP 协议栈的客户端模式 (./tcp_stack client 10.0.0.1 10001)

        - Client 发送文件 client-input.dat 给 server，server 将收到的数据存储到文件 server-output.dat

- 使用 md5sum 比较两个文件是否完全相同

- 使用 tcp_stack.py 替换其中任意一端，对端都能正确收发数据

## 二、实验设计与实现

### 实验内容一

#### `tcp_sock_read` 函数

接收 TCP 数据，从环形 buffer 中读取数据到应用。若 ring buffer 为空，则 sleep 等到收到数据包时，被唤醒；若非空，则调用 read_ring_buffer 读取数据。

```c
int tcp_sock_read(struct tcp_sock *tsk, char *buf, int len) {
	while (ring_buffer_empty(tsk->rcv_buf)) {
		if(tsk->state == TCP_CLOSE_WAIT)
			break;
		sleep_on(tsk->wait_recv);
	}

	pthread_mutex_lock(&rcv_buf_lock);
	int rlen = read_ring_buffer(tsk->rcv_buf, buf, len);
	pthread_mutex_unlock(&rcv_buf_lock);

	tsk->rcv_wnd += rlen;
	return rlen;
}
```

#### `tcp_sock_write` 函数

设置发送长度为：如果当前发送序号+待发送长度-发送确认序号大于默认窗口大小，就设置发送长度为默认窗口大小，否则设为当前发送序号+待发送长度-发送确认序号，即 `min(data_len, 1514 - ETHER_HDR_SIZE - IP_HDR_SIZE - TCP_HDR_SIZE)`。调用 tcp_send_data 从发送确认序号标记处开始发送，发送刚刚计算的发送长度的数据，调用 sleep_on（wait_send）陷入睡眠，等待被 tcp_update_window 唤醒，待发送长度减小发送长度的数值。

循环上述操作，直到数据发送完成。

```c
int tcp_sock_write(struct tcp_sock *tsk, char *buf, int len) {
	int offset = 0;
	while (len) {
		int data_len = min(len, 1514 - ETHER_HDR_SIZE - IP_BASE_HDR_SIZE - TCP_BASE_HDR_SIZE);
		int pkt_len  = data_len + ETHER_HDR_SIZE + IP_BASE_HDR_SIZE + TCP_BASE_HDR_SIZE;
		while (tsk->snd_wnd < data_len) {
			sleep_on(tsk->wait_send);
		}
		char *packet = malloc(pkt_len);
		memcpy(packet + ETHER_HDR_SIZE + IP_BASE_HDR_SIZE + TCP_BASE_HDR_SIZE, buf + offset, data_len);
		tcp_send_packet(tsk, packet, pkt_len);
		offset += data_len;
		len -= data_len;
	}
	return len;
}
```

#### 更新 `tcp_process` 函数

增加支持接收 TCP 数据包功能。如果当前状态为 ESTABLISHED，并且收到数据包不是 FIN 包，意味着要进行数据处理：如果收到的数据长度为 0，说明只是 ACK 报文，表明自己是消息发送方；否则，则是数据报文，对方为发送方，将收到的数据包中所有数据都写入到唤醒 receive buffer 中，并更新已经写入的数据量，调用 wake_up 唤醒 tcp_sock_read，最后发送 ACK 报文。

```c
	if (tsk->state == TCP_ESTABLISHED) {
		if (cb->flags & TCP_FIN) {
			tcp_set_state(tsk, TCP_CLOSE_WAIT);
			tcp_send_control_packet(tsk, TCP_ACK);
			wake_up(tsk->wait_recv);
		} else if (cb->flags & TCP_ACK) {
			tsk->rcv_nxt = cb->seq_end;
			if (cb->pl_len > 0) {
				pthread_mutex_lock(&rcv_buf_lock);
				write_ring_buffer(tsk->rcv_buf, cb->payload, cb->pl_len);
				tsk->rcv_wnd -= cb->pl_len;
				pthread_mutex_unlock(&rcv_buf_lock);
				wake_up(tsk->wait_recv);
				tcp_send_control_packet(tsk, TCP_ACK);
			} else {
				tsk->snd_wnd = cb->rwnd;
				wake_up(tsk->wait_send);
			}
		}
		return;
	}
```

### 实验内容二

修改 tcp_apps.c (以及 tcp_stack.py)，使之能够收发文件，设置每个数据包长度不超过 1000 字节。

## 三、实验结果与分析

### 实验内容一

#### Client 和标准 Server

![image-20210623103135882](15-网络传输机制实验报告二-葛忠鑫.assets/image-20210623103135882.png)

#### Server 和标准 Client

<img src="15-网络传输机制实验报告二-葛忠鑫.assets/image-20210623103444590.png" alt="image-20210623103444590" style="zoom:67%;" />

#### Server 和 Client 

![image-20210623103311282](15-网络传输机制实验报告二-葛忠鑫.assets/image-20210623103311282.png)

在三次握手连接建立完成后，h2 client 发送给 h1 server 的内容，被 h1 server 添加 “server echoes：” 报头后返回给 h2 client，共传送了十次。在10 次传输完成后，h2 client 发起关闭连接的请求，h1 server 响应。整个状态变化过程与上一次实验相同，符合预期。

### 实验内容二

#### Client 和标准 Server

![image-20210623104743361](15-网络传输机制实验报告二-葛忠鑫.assets/image-20210623104743361.png)

#### Server 和标准 Client

<img src="15-网络传输机制实验报告二-葛忠鑫.assets/image-20210623111919680.png" alt="image-20210623111919680" style="zoom:50%;" />

#### Server 和 Client 

![image-20210623103926873](15-网络传输机制实验报告二-葛忠鑫.assets/image-20210623103926873.png)

与实验一不同的，实验二的传输内容是 h2 client 打开本地的 client_input.dat 文件读入的，大小为 4052632B， h2 client 以每个包大小为 1000B 发送给 h1 server。h1 server 每次收到数据后，将数据写入到本地的 server_output.dat 中，并将计数器加 1 发送给 h2 client 。在传输完成后，双方结束连接。

调用 diff 命令比较 client_input.dat 和 server-output.dat，发现一致；并且md5sum 比较计算结果一致，说明实现了 TCP 稳定传输。

## 四、实验总结

通过这次实验，我进一步地了解了 TCP 协议中数据传输的过程。

