#!/usr/bin/python2

import sys
import string
import socket
import numpy as np
from time import sleep

data = string.digits + string.lowercase + string.uppercase

def server(port):
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    
    s.bind(('0.0.0.0', int(port)))
    s.listen(3)

    cs, addr = s.accept()
    data = ''
    while True:
        rcv = cs.recv(1000)
        if rcv:
            data += rcv
        else:
            break
    
    s.close()
    arr = []
    for i in range(len(data)):
        arr.append(data[i])
    arr = np.array(arr, dtype=str)
    arr.tofile('server-output.dat')


def client(ip, port):
    s = socket.socket()
    s.connect((ip, int(port)))
    with open("client-input.dat", "r") as f:
        while True:
            data = f.read(1000)
            if data:
                s.send(data)
                sleep(0.05)
            else:
                break
    f.close()
    s.close()

if __name__ == '__main__':
    if sys.argv[1] == 'server':
        server(sys.argv[2])
    elif sys.argv[1] == 'client':
        client(sys.argv[2], sys.argv[3])
