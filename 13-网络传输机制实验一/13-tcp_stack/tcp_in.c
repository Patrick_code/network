#include "tcp.h"
#include "tcp_sock.h"
#include "tcp_timer.h"

#include "log.h"
#include "ring_buffer.h"

#include <stdlib.h>
// update the snd_wnd of tcp_sock
//
// if the snd_wnd before updating is zero, notify tcp_sock_send (wait_send)
static inline void tcp_update_window(struct tcp_sock *tsk, struct tcp_cb *cb)
{
	u16 old_snd_wnd = tsk->snd_wnd;
	tsk->snd_wnd = cb->rwnd;
	if (old_snd_wnd == 0)
		wake_up(tsk->wait_send);
}

// update the snd_wnd safely: cb->ack should be between snd_una and snd_nxt
static inline void tcp_update_window_safe(struct tcp_sock *tsk, struct tcp_cb *cb)
{
	if (less_or_equal_32b(tsk->snd_una, cb->ack) && less_or_equal_32b(cb->ack, tsk->snd_nxt))
		tcp_update_window(tsk, cb);
}

#ifndef max
#	define max(x,y) ((x)>(y) ? (x) : (y))
#endif

// check whether the sequence number of the incoming packet is in the receiving
// window
static inline int is_tcp_seq_valid(struct tcp_sock *tsk, struct tcp_cb *cb)
{
	u32 rcv_end = tsk->rcv_nxt + max(tsk->rcv_wnd, 1);
	if (less_than_32b(cb->seq, rcv_end) && less_or_equal_32b(tsk->rcv_nxt, cb->seq_end)) {
		return 1;
	}
	else {
		log(ERROR, "received packet with invalid seq, drop it.");
		return 0;
	}
}

// Process the incoming packet according to TCP state machine. 
void tcp_process(struct tcp_sock *tsk, struct tcp_cb *cb, char *packet)
{
	// fprintf(stdout, "TODO: implement %s please.\n", __FUNCTION__);

	// TCP_CLOSED: replying TCP_RST
	if (tsk->state == TCP_CLOSED) {
		tcp_send_reset(cb);
		return;
	}

	if (tsk->state == TCP_LISTEN && (cb->flags & TCP_SYN)) {
		// malloc a child tcp sock to serve this connection request; 
		struct tcp_sock *csk = alloc_tcp_sock();
		csk->sk_sip = cb->daddr;
		csk->sk_sport = cb->dport;
		csk->sk_dip = cb->saddr;
		csk->sk_dport = cb->sport;
		csk->parent = tsk;
		csk->rcv_nxt = cb->seq + 1;
		csk->iss = tcp_new_iss();
		csk->snd_nxt = csk->iss;
		csk->state = tsk->state;
		list_add_tail(&csk->list, &tsk->listen_queue);
		tcp_set_state(csk, TCP_SYN_RECV);
		// send TCP_SYN | TCP_ACK by child tcp sock;
		tcp_send_control_packet(csk, TCP_ACK | TCP_SYN);
		// hash the child tcp sock into established_table
		tcp_hash(csk);
		return;
	}

	if (tsk->state == TCP_SYN_SENT && (cb->flags & (TCP_SYN | TCP_ACK))) {
		tsk->rcv_nxt = cb->seq + 1;
		tsk->snd_una = cb->ack;
		tcp_send_control_packet(tsk, TCP_ACK);
		tcp_set_state(tsk, TCP_ESTABLISHED);
		wake_up(tsk->wait_connect);
		return;
	}

	// check whether the sequence number of the packet is valid, if not, drop it;
	if (!is_tcp_seq_valid(tsk, cb)) {
		return;
	}
	// if the TCP_RST bit of the packet is set, close this connection, 
	// and release the resources of this tcp sock;
	if (cb->flags & TCP_RST) {
		tcp_sock_close(tsk);
		free(tsk);
		return;
	}
	// if the TCP_SYN bit is set, reply with TCP_RST and close this connection,
	// as valid TCP_SYN has been processed in state TCP_LISTEN or TCP_SYN_SENT;
	if(cb->flags & TCP_SYN) {
		tcp_send_reset(cb);
		tcp_sock_close(tsk);
		return;
	}
	// check if the TCP_ACK bit is set, since every packet (except the first SYN) should set this bit;
	if (!(cb->flags & TCP_ACK)) {
		tcp_send_reset(cb);
		return;
	}

	// process the ack of the packet: if it ACKs the outgoing SYN packet, 
	// establish the connection; (if it ACKs new data, update the window;)
	// if it ACKs the outgoing FIN packet, switch to correpsonding state;
	// (process the payload of the packet: call tcp_recv_data to receive data;)
	// if the TCP_FIN bit is set, update the TCP_STATE accordingly;
	if ((tsk->state == TCP_SYN_RECV) && (cb->flags & TCP_ACK)) {
		tcp_sock_accept_enqueue(tsk);
		tsk->rcv_nxt = cb->seq;
		tsk->snd_una = cb->ack;
		tcp_set_state(tsk, TCP_ESTABLISHED);
		wake_up(tsk->parent->wait_accept);
		return;
	}

	if (tsk->state == TCP_ESTABLISHED && (cb->flags & (TCP_FIN | TCP_ACK))) {
		tcp_update_window_safe(tsk, cb);
		tsk->rcv_nxt = cb->seq + 1;
		tcp_set_state(tsk, TCP_CLOSE_WAIT);
		tcp_send_control_packet(tsk, TCP_ACK);
		return;
	}

	if (tsk->state == TCP_FIN_WAIT_1 && (cb->flags & TCP_ACK)) {
		tcp_set_state(tsk, TCP_FIN_WAIT_2);
		return;
	}

	if (tsk->state == TCP_FIN_WAIT_2 && (cb->flags & TCP_FIN)) {
		tsk->rcv_nxt = cb->seq + 1;
		tcp_set_state(tsk, TCP_TIME_WAIT);
		tcp_set_timewait_timer(tsk);
		tcp_send_control_packet(tsk, TCP_ACK);
		tcp_unhash(tsk);
		return;
	}

	if (tsk->state == TCP_LAST_ACK && (cb->flags & TCP_ACK)) {
		tcp_set_state(tsk, TCP_CLOSED);	
		tcp_unhash(tsk);
		return;
	}
}
