/* client application */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

#define MAX_FILE_PATH 100

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Command Format: ./http_client Url\n");
        return -1;
    }

    int sock;
    struct sockaddr_in server;
    char message[1000], server_reply[2000];

    // resolve Url
    char *ip;
    int port;
    char *file_name;
    FILE *fd;

    char *url[40];
    url[0] = strtok(argv[1], "/");
    int i = 1;
    while ((url[i] = strtok(NULL, "/")) != NULL)
    {
        i++;
    }
    ip = strtok(url[1], ":"); // skip "http:" and delete ":80"
    port = atoi(strtok(NULL, ":"));
    file_name = url[i - 1];

    // set file path
    char file_path[MAX_FILE_PATH] = "";
    strcat(file_path, ".");
    for (int j = 2; j < i; j++)
    {
        strcat(file_path, "/");
        strcat(file_path, url[j]);
    }

    // set receive path
    char recv_path[MAX_FILE_PATH] = "";
    strcat(recv_path, "./client_recv/");
    strcat(recv_path, file_name);

    printf("ip: %s port: %d\n", ip, port);
    printf("file_path: %s\n", file_path);
    // printf("%s\n", file_path + 2);
    printf("recv_path: %s\n", recv_path);

    // create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1)
    {
        printf("create socket failed\n");
        return -1;
    }
    printf("socket created\n");

    server.sin_addr.s_addr = inet_addr(ip);
    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    // connect to server
    if (connect(sock, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("connect failed\n");
        return -1;
    }
    printf("connected\n");

    // construct the http head
    strcat(message, "GET /");
    strcat(message, file_path + 2);
    strcat(message, "  HTTP/1.1\r\n");
    strcat(message, "\r\n");
    printf("http head: %s", message);

    // send data
    if (send(sock, message, strlen(message), 0) < 0)
    {
        printf("send failed\n");
        return -1;
    }

    int file_length = 0;
    int received_size = 0;
    while (1)
    {
        static int count = 1;

        int len = recv(sock, server_reply, 2000, 0);
        // fail
        if (len <= 0 && count == 1)
        {
            printf("\nrecv failed");
            close(sock);
            return -1;
        }

        // error 404 file not found
        // compatible with "python -m SimpleHTTPServer 80"
        if (strstr(server_reply, "404 File not found"))
        {
            printf("\t[ERROR]HTTP 404 Not Found\n");
            close(sock);
            return -1;
        }

        // success
        server_reply[len] = 0;
        printf("len: %d server reply: %s", len, server_reply);
        if (count == 1)
        {
            // open recv file
            if ((fd = fopen(recv_path, "w")) == NULL)
            {
                printf("open file failed\n");
                return -1;
            };
            printf("server reply : \n");
            // get msg length
            char *length_start = server_reply;
            if ((length_start = strstr(server_reply, "Content-Length: ")) != NULL)
            {
                length_start += strlen("Content-Length: ");
                char length_str[20];
                int k = 0;
                for (; length_start[k] != '\r' && k < 20; k++)
                    length_str[k] = length_start[k];
                length_str[k] = 0;
                file_length = atoi(length_str);
            }
            // get where msg start
            char *start = server_reply;
            if ((start = strstr(server_reply, "\r\n\r\n")) != NULL)
            {
                start += 4;
                // printf("server_reply: %lx start: %lx\n", server_reply, start);
                // printf("msg_len: %d\n", server_reply + len - start);
                fwrite(start, server_reply + len - start, 1, fd);
                received_size += server_reply + len - start;
            }
        }
        else
        {
            fwrite(server_reply, len, 1, fd);
            received_size += len;
        }
        if (received_size == file_length)
        {
            break;
        }
        count++;
    }
    printf("received: %d, file_size: %d\n", received_size, file_length);
    printf("receive done\n");

    fclose(fd);
    close(sock);

    return 0;
}