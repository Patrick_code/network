#include "tcp.h"
#include "tcp_sock.h"
#include "tcp_timer.h"

#include "log.h"
#include "ring_buffer.h"

#include <stdlib.h>
// update the snd_wnd of tcp_sock
//
// if the snd_wnd before updating is zero, notify tcp_sock_send (wait_send)
static inline void tcp_update_window(struct tcp_sock *tsk, struct tcp_cb *cb)
{
	u16 old_snd_wnd = tsk->snd_wnd;
	tsk->snd_wnd = cb->rwnd;
	if (old_snd_wnd == 0)
		wake_up(tsk->wait_send);
}

// update the snd_wnd safely: cb->ack should be between snd_una and snd_nxt
static inline void tcp_update_window_safe(struct tcp_sock *tsk, struct tcp_cb *cb)
{
	if (less_or_equal_32b(tsk->snd_una, cb->ack) && less_or_equal_32b(cb->ack, tsk->snd_nxt))
		tcp_update_window(tsk, cb);
}

#ifndef max
#	define max(x,y) ((x)>(y) ? (x) : (y))
#endif

// check whether the sequence number of the incoming packet is in the receiving
// window
static inline int is_tcp_seq_valid(struct tcp_sock *tsk, struct tcp_cb *cb)
{
	u32 rcv_end = tsk->rcv_nxt + max(tsk->rcv_wnd, 1);
	if (less_than_32b(cb->seq, rcv_end) && less_or_equal_32b(tsk->rcv_nxt, cb->seq_end)) {
		return 1;
	}
	else {
		log(ERROR, "received packet with invalid seq, drop it.");
		return 0;
	}
}

// Process the incoming packet according to TCP state machine. 
void tcp_process(struct tcp_sock *tsk, struct tcp_cb *cb, char *packet)
{
	// fprintf(stdout, "TODO: implement %s please.\n", __FUNCTION__);
	// if the TCP_RST bit of the packet is set, close this connection, 
	// and release the resources of this tcp sock;
	if (cb->flags & TCP_RST) {
		tcp_sock_close(tsk);
		return;
	}

	// TCP_SYN: processed in state TCP_LISTEN && TCP_SYN_SENT;
	if (cb->flags & TCP_SYN) {
		if ((cb->flags & TCP_ACK) && tsk->state == TCP_SYN_SENT) {
			tsk->rcv_nxt = cb->seq_end;
			wake_up(tsk->wait_connect);
			tcp_send_control_packet(tsk, TCP_ACK);
			tcp_set_state(tsk, TCP_ESTABLISHED);
		}
		else if (tsk->state == TCP_LISTEN) {
			struct tcp_sock *csk = alloc_tcp_sock();
			csk->sk_sip = cb->daddr;
			csk->sk_sport = cb->dport;
			csk->sk_dip = cb->saddr;
			csk->sk_dport = cb->sport;
			csk->parent = tsk;
			csk->iss = tcp_new_iss();
			csk->snd_una = csk->iss;
			csk->snd_nxt = csk->iss;
			csk->rcv_nxt = cb->seq_end;
			csk->state = tsk->state;
			tcp_set_state(csk, TCP_SYN_RECV);
			tcp_sock_listen_enqueue(csk);
			// hash the child tcp sock into established_table
			tcp_hash(csk);
			struct pkt_block_buf *dblk = alloc_pkt_block_buf();
			init_pkt_block_buf(dblk, TCP_SYN | TCP_ACK, csk->snd_nxt, 0, NULL);
			// send TCP_SYN | TCP_ACK by child tcp sock;
			tcp_send_control_packet(csk, TCP_SYN | TCP_ACK);
			pthread_mutex_lock(&send_buf_lock);
			list_add_tail(&dblk->list, &csk->send_buf.list);
			pthread_mutex_unlock(&send_buf_lock);
			if (!csk->retrans_timer.enable) {
				tcp_set_retrans_timer(csk);
			}
		}
	}

	// check whether the sequence number of the packet is valid, if not, drop it;
	if (!is_tcp_seq_valid(tsk, cb)) {
		return;
	}

	// process the ack of the packet: if it ACKs the outgoing SYN packet, 
	// establish the connection; (if it ACKs new data, update the window;)
	// if it ACKs the outgoing FIN packet, switch to correpsonding state;
	// (process the payload of the packet: call tcp_recv_data to receive data;)
	// if the TCP_FIN bit is set, update the TCP_STATE accordingly;
	if (cb->flags & TCP_ACK) {
		if (tsk->state == TCP_ESTABLISHED) {
			tsk->snd_una = max(tsk->snd_una, cb->ack);
			tsk->adv_wnd = cb->rwnd;
			tsk->snd_wnd = cb->rwnd;
			wake_up(tsk->wait_send);
			if (cb->pl_len > 0) {
				if (tsk->rcv_nxt != cb->seq) {
					tcp_rcv_ofo_pkt(tsk, cb);
					tcp_send_control_packet(tsk, TCP_ACK);
					tcp_free_send_buf(tsk, cb);
					return;
				}
				wake_up(tsk->wait_recv);
				pthread_mutex_lock(&rcv_buf_lock);
				write_ring_buffer(tsk->rcv_buf, cb->payload, cb->pl_len);
				tsk->rcv_wnd -= cb->pl_len;
				pthread_mutex_unlock(&rcv_buf_lock);
				tsk->rcv_nxt = cb->seq_end;
				u8 fin_flag = 0;
				struct pkt_block_buf *tmp, *q;
				list_for_each_entry_safe(tmp, q, &tsk->rcv_ofo_buf.list, list) {
					if (tmp->seq == tsk->rcv_nxt) {
						wake_up(tsk->wait_recv);
						pthread_mutex_lock(&rcv_buf_lock);
						write_ring_buffer(tsk->rcv_buf, tmp->packet, tmp->len);
						tsk->rcv_wnd -= tmp->len;
						pthread_mutex_unlock(&rcv_buf_lock);
						tsk->rcv_nxt = tmp->seq_end;
						fin_flag = tmp->flags & TCP_FIN;
						list_delete_entry(&tmp->list);
						free(tmp->packet);
						free(tmp);
					}
					else {
						break;
					}
				}
				
				if (fin_flag) {
					tcp_send_control_packet(tsk, TCP_ACK);
					tcp_set_state(tsk, TCP_CLOSE_WAIT);
					wake_up(tsk->wait_recv);
				}
				else {
					tcp_send_control_packet(tsk, TCP_ACK);
				}
			}
		}
		else if (tsk->state == TCP_SYN_RECV) {
			tsk->rcv_nxt = cb->seq_end;
			tcp_set_state(tsk, TCP_ESTABLISHED);
			tcp_sock_accept_enqueue(tsk);
			wake_up(tsk->parent->wait_accept);
		}
		else if (tsk->state == TCP_FIN_WAIT_1) {
			tsk->rcv_nxt = cb->seq_end;
			tcp_set_state(tsk, TCP_FIN_WAIT_2);
		}
		else if (tsk->state == TCP_LAST_ACK) {
			tsk->rcv_nxt = cb->seq_end;
			tcp_set_state(tsk, TCP_CLOSED);
		}
		tcp_free_send_buf(tsk, cb);
	}

	if (cb->flags & TCP_FIN) {
		if (cb->seq_end == tsk->rcv_nxt || cb->seq == tsk->rcv_nxt) {
			tsk->rcv_nxt = cb->seq_end;
			if (tsk->state == TCP_ESTABLISHED) {
				tcp_send_control_packet(tsk, TCP_ACK);
				tcp_set_state(tsk, TCP_CLOSE_WAIT);
				wake_up(tsk->wait_recv);
			}
			else if (tsk->state == TCP_FIN_WAIT_2) {
				tcp_send_control_packet(tsk, TCP_ACK);
				tcp_set_state(tsk, TCP_TIME_WAIT);
				tcp_set_timewait_timer(tsk);
			}
		}
	}
}
