#include "tcp_sock.h"

#include "log.h"

#include <stdlib.h>
#include <unistd.h>

#define FILE_NOT_FOUND "HTTP/1.1 404 File not found\r\n\r\n"
#define FILE_FOUND "HTTP/1.1 200 OK\r\n"
#define MAX_LEN 1000

// get file size
int file_size(char *filename)
{
    FILE *fp = fopen(filename, "r");
    if (!fp)
        return -1;
    fseek(fp, 0L, SEEK_END);
    int size = ftell(fp);
    fclose(fp);
    return size;
}

int handle_request(struct tcp_sock * csk)
{
    // printf("%d: connection accepted\n", cs);

    FILE *fd;
    char msg[2000];
    char file_path[100] = "";

    int msg_len = 0;
    // receive a message from client
    // while ((msg_len = recv(cs, msg, sizeof(msg), 0)) > 0)
    while ((msg_len = tcp_sock_read(csk, msg, 1400)) > 0)
    {
        msg[msg_len] = 0;
        printf("%s\n", msg);

        // resolve url(file path)
        char *url[20];
        url[0] = strtok(msg, " ");
        int i = 1;
        while ((url[i] = strtok(NULL, " ")) != NULL)
        {
            i++;
        }
        strcat(file_path, ".");
        strcat(file_path, url[1]);

        printf("file_path: %s\n", file_path);
        if ((fd = fopen(file_path, "r")) == NULL)
        {
            printf("File Not Found\n");
            // send 404 back to client
            // write(cs, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND));
            tcp_sock_write(csk, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND));
            return -1;
        }
        int fsize = file_size(file_path);
        char fsize_str[20];
        sprintf(fsize_str, "%d", fsize);

		printf(">>>>>>>>.\n");
        memset(msg, 0, sizeof(msg));
        // add http head
        strcat(msg, FILE_FOUND);
        strcat(msg, "Content-Length: ");
        strcat(msg, fsize_str);
        strcat(msg, "\r\n");
        strcat(msg, "Connection: Keep-Alive\r\n\r\n");
        // send the message back to client
        // write(cs, msg, strlen(msg));
        tcp_sock_write(csk, msg, strlen(msg));


        // send data
        while (1)
        {
            memset(msg, 0, sizeof(msg));
            fgets(msg, sizeof(msg), fd);
            // if (send(cs, msg, strlen(msg), 0) < 0)
            // {
            //     printf("send Failed");
            //     return 1;
            // }
            tcp_send_packet(csk, msg, strlen(msg));
            if (feof(fd))
                break;
        }

        fclose(fd);
    }

    if (msg_len == 0)
    {
        printf("client disconnected\n");
    }
    else
    { // msg_len < 0
        perror("recv failed\n");
        return -1;
    }

    tcp_sock_close(csk);
    // printf("%d: request done\n", cs);

    return 0;
}


// tcp server application, listens to port (specified by arg) and serves only one
// connection request
void *tcp_server(void *arg)
{
    // int sockfd;
    // struct sockaddr_in server, client;

    // // create socket
    // if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    // {
    //     perror("create socket failed\n");
    //     return -1;
    // }
    // printf("socket created\n");

    // // prepare the sockaddr_in structure
    // server.sin_family = AF_INET;
    // server.sin_addr.s_addr = INADDR_ANY;
    // server.sin_port = htons(atoi(argv[1]));
    struct tcp_sock *tsk = alloc_tcp_sock();
    struct sock_addr addr;
    addr.ip = htonl(0);
    addr.port = htons(*(u16 *)arg);

    // bind
    // if (bind(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0)
    // {
    //     perror("bind failed\n");
    //     return -1;
    // }
    // printf("bind done\n");
	if (tcp_sock_bind(tsk, &addr) < 0)
	{
		log(ERROR, "tcp_sock bind to port %hu failed", ntohs(addr.port));
		exit(1);
	}

    // listen
    // listen(sockfd, 128);
	if (tcp_sock_listen(tsk, 3) < 0)
	{
		log(ERROR, "tcp_sock listen failed");
		exit(1);
	}

    while (1)
    {
        // int c = sizeof(struct sockaddr_in);
        printf("waiting for incoming connections...\n");
        // int request = accept(sockfd, (struct sockaddr *)&client, (socklen_t *)&c);
        struct tcp_sock *csk = tcp_sock_accept(tsk);
        // if (request < 0) {
        //     perror("accept failed\n");
        //     continue;
        // }
        log(DEBUG, "accept a connection.");

        pthread_t thread;
        pthread_create(&thread, NULL, (void *)handle_request, (void *)(intptr_t)csk);
        pthread_detach(pthread_self());
    }
}

// tcp client application, connects to server (ip:port specified by arg), each
// time sends one bulk of data and receives one bulk of data
void *tcp_client(void *arg)
{
	struct sock_addr *skaddr = arg;

	struct tcp_sock *tsk = alloc_tcp_sock();

	if (tcp_sock_connect(tsk, skaddr) < 0)
	{
		log(ERROR, "tcp_sock connect to server (" IP_FMT ":%hu)failed.",
			NET_IP_FMT_STR(skaddr->ip), ntohs(skaddr->port));
		exit(1);
	}

	FILE *fp;
	if ((fp = fopen("client-input.dat", "r")) == NULL)
	{
		printf("Open File Falied!\n");
		return NULL;
	}

	char wbuf[MAX_LEN + 1];

	u32 total_wsize = 0;

	while (!feof(fp))
	{
		u32 wlen = fread(wbuf, 1, MAX_LEN, fp);
		if (tcp_sock_write(tsk, wbuf, wlen) < 0) {
			break;
		}
		total_wsize += wlen;
		printf("Sent %u bytes\n", total_wsize);
	}

	sleep(1);

	fclose(fp);

	tcp_sock_close(tsk);

	return NULL;
}
