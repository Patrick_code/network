#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <time.h>

#define FILE_NOT_FOUND "HTTP/1.1 404 File not found\r\n\r\n"
#define FILE_FOUND "HTTP/1.1 200 OK\r\n"
#define MAX_FILE_PATH 100
#define THREAD_NUM 8

int file_size(char *filename);
int handle_request(int cs);

int main(int argc, const char *argv[])
{

    int sockfd;
    struct sockaddr_in server, client;

    // create socket
    if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        perror("create socket failed\n");
        return -1;
    }
    printf("socket created\n");

    // prepare the sockaddr_in structure
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons(atoi(argv[1]));

    // bind
    if (bind(sockfd, (struct sockaddr *)&server, sizeof(server)) < 0)
    {
        perror("bind failed\n");
        return -1;
    }
    printf("bind done\n");

    // listen
    listen(sockfd, 128);

    while (1)
    {
        int c = sizeof(struct sockaddr_in);
        printf("waiting for incoming connections...\n");
        int request = accept(sockfd, (struct sockaddr *)&client, (socklen_t *)&c);
        if (request < 0) {
            perror("accept failed\n");
            continue;
        }

        pthread_t thread;
        pthread_create(&thread, NULL, (void *)handle_request, (void *)(intptr_t)request);
        pthread_detach(pthread_self());
    }

    return 0;
}

// get file size
int file_size(char *filename)
{
    FILE *fp = fopen(filename, "r");
    if (!fp)
        return -1;
    fseek(fp, 0L, SEEK_END);
    int size = ftell(fp);
    fclose(fp);
    return size;
}

int handle_request(int cs)
{
    printf("%d: connection accepted\n", cs);

    FILE *fd;
    char msg[2000];
    char file_path[MAX_FILE_PATH] = "";

    int msg_len = 0;
    // receive a message from client
    while ((msg_len = recv(cs, msg, sizeof(msg), 0)) > 0)
    {
        msg[msg_len] = 0;
        printf("%s\n", msg);

        // resolve url(file path)
        char *url[20];
        url[0] = strtok(msg, " ");
        int i = 1;
        while ((url[i] = strtok(NULL, " ")) != NULL)
        {
            i++;
        }
        strcat(file_path, ".");
        strcat(file_path, url[1]);

        printf("file_path: %s\n", file_path);
        if ((fd = fopen(file_path, "r")) == NULL)
        {
            printf("File Not Found\n");
            // send 404 back to client
            write(cs, FILE_NOT_FOUND, strlen(FILE_NOT_FOUND));
            return -1;
        }
        int fsize = file_size(file_path);
        char fsize_str[20];
        sprintf(fsize_str, "%d", fsize);

        memset(msg, 0, sizeof(msg));
        // add http head
        strcat(msg, FILE_FOUND);
        strcat(msg, "Content-Length: ");
        strcat(msg, fsize_str);
        strcat(msg, "\r\n");
        strcat(msg, "Connection: Keep-Alive\r\n\r\n");
        // send the message back to client
        write(cs, msg, strlen(msg));

        // send data
        while (1)
        {
            memset(msg, 0, sizeof(msg));
            fgets(msg, sizeof(msg), fd);
            if (send(cs, msg, strlen(msg), 0) < 0)
            {
                printf("send Failed");
                return 1;
            }
            if (feof(fd))
                break;
        }
        fclose(fd);
    }

    if (msg_len == 0)
    {
        printf("client disconnected\n");
    }
    else
    { // msg_len < 0
        perror("recv failed\n");
        return -1;
    }

    printf("%d: request done\n", cs);

    return 0;
}
