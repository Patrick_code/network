#include "ip.h"
#include "icmp.h"

#include <stdio.h>
#include <stdlib.h>

// handle ip packet
//
// If the packet is ICMP echo request and the destination IP address is equal to
// the IP address of the iface, send ICMP echo reply; otherwise, forward the
// packet.
void handle_ip_packet(iface_info_t *iface, char *packet, int len)
{
	// fprintf(stderr, "TODO: handle ip packet.\n");
	struct iphdr *ip_hdr = packet_to_ip_hdr(packet);
	struct icmphdr *icmp_hdr = (struct icmphdr *)(packet + ETHER_HDR_SIZE + IP_HDR_SIZE(ip_hdr));

	if (ip_hdr->protocol == IPPROTO_ICMP && icmp_hdr->type == ICMP_ECHOREQUEST && ntohl(ip_hdr->daddr) == iface->ip)
	{
		// the packet is ICMP echo request &&
		// the destination IP address is equal to the IP address of the iface, send ICMP echo reply
		// ICMP type: ICMP_ECHOREPLY(0), code: ICMP_NET_UNREACH(0)
		icmp_send_packet(packet, len, ICMP_ECHOREPLY, ICMP_NET_UNREACH);
	}
	else
	{
		ip_send_packet(packet, len);
	}
}