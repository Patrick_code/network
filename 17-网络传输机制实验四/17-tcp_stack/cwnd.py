import matplotlib.pyplot as plt

line = open('cwnd.dat').readlines()

p = []
for i in range(len(line)):
    if int(line[i].split()[0]) > 1460:
        p.append(line[i])
p.pop()

x = list(range(len(p)))
for i in range(len(p)):
    x[i] = int(p[i].split()[1]) / 1000000
    p[i] = int(p[i].split()[0])

plt.xlabel("time(s)")
plt.ylabel("cwnd # bytes")
plt.plot(x, p)
plt.show()
